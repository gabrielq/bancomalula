package br.com.bancomalula;

public class BancoTeste {

	public static void main(String[] args) {
		// pra comentar tudo de uma vez = seleciona tudo e dps aplica CTRL + SHIFT + C
		Conta.saldoDoBanco = 5_000.00;
		
		// INSTANCIANDO UMA CONTA CORRENTE
		ContaCorrente cc = new ContaCorrente(1, 500.00, "123!", new Cliente("Gabriel", "3956854659", "15935745269", "gabriel@senai.com", Sexo.MASCULINO));
		// INSTANCIANDO UMA CONTA POUPAN�A
		ContaPoupanca cp = new ContaPoupanca(1, 100.00, "789!", new Cliente("Lucas", "1594785212", "12345678998", "lucas@outlook.com", Sexo.MASCULINO));
		
		// DADOS DAS CONTAS 
		System.out.println("   DADOS DA CONTA    ");
		System.out.println("N�:      " + cc.getNumero());
		System.out.println("Saldo:   " + cc.getSaldo());
		System.out.println("Senha:   " + cc.getSenha());
		System.out.println("Titular: " + cc.getCliente().getNome());
		System.out.println("RG:      " + cc.getCliente().getRg());
		System.out.println("CPF:     " + cc.getCliente().getCpf());
		System.out.println("E-mail:  " + cc.getCliente().getEmail());
		System.out.println("Sexo:    " + cc.getCliente().getSexo().nome);
		
//		Conta conta = new Conta(1, "Corrente", 2_750.00, "123!", new Cliente("Gabriel", "3956847823", "15975365428", "gabriel@senai.com", Sexo.MASCULINO));
//		
//		// operatizando contas
//		conta.exibeSaldo();
//		conta.deposita(50);
//		System.out.println("Cofre: " + Conta.saldoDoBanco);
//		conta.exibeSaldo();
//		conta.saca(100);
//		conta.exibeSaldo();
//		System.out.println("Cofre: " + Conta.saldoDoBanco);
//		System.out.println();
//		
//		// mostrar os dados da conta
//		System.out.println("  DADOS DA CONTA  ");
//		System.out.println("N�:      " + conta.getNumero());
//		System.out.println("Tipo:    " + conta.getTipo());
//		System.out.println("Senha:   " + conta.getSenha());
//		System.out.println("Saldo:   " + conta.getSaldo());
//		System.out.println("Titular: " + conta.getCliente().getNome());
//		System.out.println("RG:      " + conta.getCliente().getRg());
//		System.out.println("CPF:     " + conta.getCliente().getCpf());
//		System.out.println("Email:   " + conta.getCliente().getEmail());
//		System.out.println("Sexo:    " + conta.getCliente().getSexo().nome);
	}
}