package br.com.bancomalula;

/**
 * 
 * @author Gabriel Quintas
 *
 */

public class Conta {

	// vari�vel global
	public static double saldoDoBanco;
	
	// atributos
	private int numero;
	private double saldo;
	private String senha;
	private Cliente cliente;
	
	// construtor
	public Conta(int numero,double saldo, String senha, Cliente cliente) {
		this.numero = numero;
		this.saldo = saldo;
		this.senha = senha;
		this.cliente = cliente;
	}

	// getters e setters
	// N�MERO
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}

	// SALDO
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	// SENHA
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	// CLIENTE
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	// m�todos
	// EXIBE SALDO
	public void exibeSaldo() {
		System.out.println(cliente.getNome() + ", seu saldo � de R$ " + this.getSaldo());
	}
	
	// SACA
	public void saca(double valor) {
		this.saldo -= valor;
		Conta.saldoDoBanco -= valor;
	}
	
	// DEPOSITA
	public void deposita(double valor) {
		this.saldo += valor;
		Conta.saldoDoBanco += valor;
	}
	
	// TRANSFERE PARA
	public void transferePara(Conta destino, double valor) {
		this.saca(valor);
		destino.deposita(valor);
	}
		
}